import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { HomeComponent } from './home/home/home.component';
import { CheckResultsService } from './results/services/check-results.service';

const ROUTES: Routes = [
  // El orden importa, si hay un path que coincide antes que otro, va a ir a ese
  { path: '', component: HomeComponent, pathMatch: 'full' },
  // { path: 'chuck', component: ChuckComponent },
  {
    path: 'questionnaire',
    loadChildren: () => import('./questionnaire/questionnaire.module').then(m => m.QuestionnaireModule),
    data: { title: 'Questionnaire' }
  },
  {
    path: 'results',
    canActivate: [CheckResultsService],
    loadChildren: () => import('./results/results.module').then(m => m.ResultsModule),
    data: { title: 'Results' }
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES)
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
