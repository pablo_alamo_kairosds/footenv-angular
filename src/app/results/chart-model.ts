export interface Chart {
  percentage: number,
  category: string,
  color: string;
}

export interface Contribution {
  co2e: number,
  category: string;
}