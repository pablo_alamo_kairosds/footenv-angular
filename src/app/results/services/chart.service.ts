import { Injectable } from '@angular/core';
import { Result } from 'src/app/questionnaire/model';
import { Chart, Contribution } from '../chart-model';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor() { }

  getCharts(results: Result[], totCO2: number): Chart[] {
    const cO2Contributions: Array<Contribution> = results.map((result: Result) => {
      let resultCategory: string = result.emission_factor.category;
      if (resultCategory === 'Clothing and Footwear') {
        resultCategory = 'Clothes';
      }
      return {
        co2e: result.co2e,
        category: resultCategory
      };
    }).sort((a, b) => b.co2e - a.co2e);

    return cO2Contributions.map((contribution, index, array) => {
      const percentage = Math.trunc(((contribution.co2e / 1000) / totCO2) * 100);
      let color: string = '';
      if (index === 0) {
        color = '#ff0000';
      } else if (index === array.length - 1) {
        color = '#00d900';
      } else {
        color = '#ffaa00';
      }
      return {
        percentage: percentage,
        category: contribution.category,
        color: color
      };
    }, []);
  };
}
