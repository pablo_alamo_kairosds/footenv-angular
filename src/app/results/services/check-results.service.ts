import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { catchError, map } from 'rxjs/operators';
import { Result } from '../../questionnaire/model';
import { QUESTIONS } from '../../questionnaire/questions';
import { FetchService } from '../../questionnaire/services/fetch-results.service';

@Injectable({ providedIn: 'root' })
export class CheckResultsService implements CanActivate {

  constructor(private router: Router,
    private fetchService: FetchService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    return this.fetchService.hasResults$().pipe(
      map((results: Result[]) => {
        const questionsWithoutNoFetchProp = QUESTIONS.filter((question) => !question.emission_factor.hasOwnProperty('nofetch'));

        console.log("---> CHECK:", results);
        if (results && Object.keys(results).length === questionsWithoutNoFetchProp.length) {
          return true;
        } else {
          throw new Error();
        }
      }), catchError(() => {
        return of(this.router.createUrlTree(['']));
      })
    );
  }

}
