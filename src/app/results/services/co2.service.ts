import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Result } from 'src/app/questionnaire/model';
import { TruncService } from 'src/app/questionnaire/services/trunc.service';

@Injectable({
  providedIn: 'root'
})
export class Co2Service {

  constructor(private truncService: TruncService) { }

  getCO2$(results: Result[]): Observable<number> {
    return of(this.truncService.truncNumber(
      results.reduce((acc, currentResult) => acc + currentResult.co2e!, 0) / 1000
    ));
  }

}
