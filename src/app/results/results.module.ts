import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UiModule } from '../ui/ui.module';
import { ResultsComponent } from './results/results.component';
import { TreesPipe } from './trees.pipe';

const ROUTES: Routes = [
  // Podría ser path: 'directives/:id' para unos detalles o 'directives/first' para mandar a una específica
  { path: '', component: ResultsComponent, pathMatch: 'full' },
];

@NgModule({
  declarations: [
    ResultsComponent,
    TreesPipe
  ],
  imports: [
    CommonModule,
    UiModule,
    RouterModule.forChild(ROUTES)
  ]
})
export class ResultsModule { }
