import { AfterContentInit, Component, OnInit } from '@angular/core';
import { Result } from 'src/app/questionnaire/model';
import { Co2StoreService } from 'src/app/state/co2-store.service';
import { ResultsStoreService } from 'src/app/state/results-store.service';
import { Chart } from '../chart-model';
import { ChartService } from '../services/chart.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit, AfterContentInit {

  results: Result[];
  charts: Chart[];
  co2: number;

  constructor(private resultsStore: ResultsStoreService,
    private co2Store: Co2StoreService,
    private chartsService: ChartService) { }

  ngOnInit(): void {
    this.results = this.resultsStore.get();
  }

  ngAfterContentInit(): void {
    this.co2Store.init(this.results);
    this.co2 = this.co2Store.get();
    this.charts = this.chartsService.getCharts(this.results, this.co2);

    console.log(`co2:${ this.co2 }`);
    console.log("charts:", this.charts);
  }

}
