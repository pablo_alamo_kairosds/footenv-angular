import { Pipe, PipeTransform } from '@angular/core';
// import { memoize } from 'cypress/types/lodash';
// const decoratorFunc = memoize((value: string, dec: string): string => {
//   return `${ dec } ${ value }`;
// });

const TREES_PER_TONNE_OF_CO2: number = 27;

@Pipe({
  name: 'trees'
})
export class TreesPipe implements PipeTransform {

  transform(value: number): number {
    return Math.trunc(TREES_PER_TONNE_OF_CO2 * value);
  }

}
