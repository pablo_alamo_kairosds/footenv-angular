import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartComponent } from './chart/chart.component';
import { HomeCardComponent } from './home-card/home-card.component';

@NgModule({
  declarations: [
    ChartComponent,
    HomeCardComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [ChartComponent, HomeCardComponent]
})
export class UiModule { }
