import { Component, Input } from '@angular/core';
import { HomeCard } from 'src/app/home/home/home-card-model';

@Component({
  selector: 'app-home-card',
  templateUrl: './home-card.component.html',
  styleUrls: ['./home-card.component.css']
})
export class HomeCardComponent {

  @Input() card: HomeCard;

  constructor() { }

}
