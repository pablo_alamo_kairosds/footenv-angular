import { AfterViewInit, Component, ElementRef, Input, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Chart } from 'src/app/results/chart-model';
import { AnimationTuple } from '../../questionnaire/animations';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('circle') circle: ElementRef;
  @Input() chart: Chart;
  counter: number;
  CHART_ANIM: AnimationTuple;
  subscription: Subscription;
  interval: any;

  constructor(private ngZone: NgZone) { }

  ngOnInit(): void {
    const CHART_ANIM_KEYFRAMES: Keyframe[] = [
      { strokeDashoffset: 440 - (440 * this.chart.percentage / 100) }
    ];
    const CHART_ANIM_OPTS: KeyframeAnimationOptions = {
      duration: 1000,
      easing: 'linear',
      fill: 'forwards'
    };
    this.CHART_ANIM = [
      CHART_ANIM_KEYFRAMES,
      CHART_ANIM_OPTS
    ];

    this.counter = 0;
  }

  ngAfterViewInit(): void {
    // this.subscription = interval(1000 / this.chart.percentage)
    //   .subscribe(() => {
    //     if (this.counter === this.chart.percentage) {
    //       this.subscription.unsubscribe();
    //     } else {
    //       this.ngZone.run(() => {
    //         this.counter += 1;
    //         console.log(this.counter);
    //       });
    //     }
    //   });
    this.interval = setInterval(() => {
      if (this.counter === this.chart.percentage) {
        clearInterval(this.interval);
      } else {
        this.ngZone.run(() => {
          this.counter += 1;
          console.log(this.counter);
        });
      }
    }, 1000 / this.chart.percentage);

    this.circle.nativeElement.animate(...this.CHART_ANIM);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
    // this.subscription.unsubscribe();
  }
}
