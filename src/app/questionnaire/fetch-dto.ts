export interface FetchDTO {
  results: ResultDTO[];
}

export interface ResultDTO {
  co2e: number;
  co2e_unit: string;
  emission_factor: EmissionFactorDTO;
  co2e_calculation_method: string;
  co2e_calculation_origin: string;
  constituent_gases: ConstituentGasesDTO;
}

export interface ConstituentGasesDTO {
  co2e_total: number;
  co2e_other: any;
  co2: any;
  ch4: any;
  n2o: any;
}

export interface EmissionFactorDTO {
  _id: string;
  region: string;
  category: string;
  source: string;
  year: string;
  lca_activity: string;
}