import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';

const ROUTES: Routes = [
  { path: '', component: QuestionnaireComponent, pathMatch: 'full' },
];

@NgModule({
  declarations: [
    QuestionnaireComponent,
    FormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QuestionnaireModule { }
