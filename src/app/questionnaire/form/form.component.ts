import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'genk-step-progress-bar/genk-step-progress-bar.js';
import { ANIMATIONS, MyAnimation } from '../animations';
import { Answer, Question } from '../model';
import { QUESTIONS } from '../questions';
import { RequestService } from '../services/request.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormComponent implements OnInit, AfterViewInit {

  form: FormGroup;

  @Input() slideNumber: number;
  @Output() answerChecked = new EventEmitter();
  @Output() prevEvent = new EventEmitter();
  @Output() submitEvent = new EventEmitter();

  @ViewChild('prevSlideBtn') prevSlideBtn: ElementRef;
  @ViewChild('progressBar') progressBar: ElementRef;
  @ViewChild('carousel') carousel: ElementRef;

  questions: Question[];
  myAnimations: MyAnimation;
  stepNumber: number;

  answers: Map<String, Answer>;

  constructor(private requestService: RequestService) { }

  ngOnInit(): void {
    this.form = new FormGroup({

    });

    this.questions = QUESTIONS;
    this.myAnimations = ANIMATIONS;
    this.stepNumber = 1;
    this.answers = new Map();
  }

  ngAfterViewInit(): void {
    this.prevSlideBtn.nativeElement.animate(...this.myAnimations.slideEnter);
    this.progressBar.nativeElement.animate(...this.myAnimations.slideEnter);
  }

  checkAnswer(ev: any): void {
    if (this.stepNumber <= this.questions.length) {
      this.stepNumber += 1;
    }
    this.answerChecked.emit();

    const answer = ev.target.value;

    // console.log(this.form.value);

    this.saveAnswer(answer);
  }

  saveAnswer(answer: string): void {
    const { emission_factor, parameters } = this.requestService.execute(this.questions, answer);

    this.answers.set(emission_factor._id!, { emission_factor, parameters });

    if (this.questions.length === this.answers.size) {
      this.prevSlideBtn.nativeElement.animate(...this.myAnimations.slideLeave);
      this.carousel.nativeElement.animate(...this.myAnimations.slideLeave);
      setTimeout(() => {
        this.progressBar.nativeElement.animate(...this.myAnimations.slideLeave);
        this.sendRequest();
      }, 1000);
    }
  }

  sendRequest(): void {
    const answersValues: Answer[] = [...this.answers.values()];
    this.submitEvent.emit(answersValues);
  }

  goToPrevSlide(): void {
    if (this.stepNumber > 1) {
      this.stepNumber -= 1;
    }
    this.prevEvent.emit();
  }

}
