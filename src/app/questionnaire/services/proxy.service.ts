import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { FetchDTO } from '../fetch-dto';
import { Answer } from '../model';

@Injectable({
  providedIn: 'root'
})
export class ProxyService {

  constructor(private http: HttpClient) { }

  getFetchResults(body: Answer[]): Observable<FetchDTO> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer WHGR11YNSJ4AZHGXY4BPRX7JH2FY');

    return this.http.post<FetchDTO>('https://beta3.api.climatiq.io/batch', body, {
      headers: headers
    });
  }

}
