import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Result } from '../model';

@Injectable({
  providedIn: 'root'
})
export class ObservableResultsService {

  constructor() { }

  getResults$(results: Result[]) {
    return of(results);
  }

}
