import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { expect } from '@jest/globals';
import { FetchDTO } from '../fetch-dto';
import { Answer } from '../model';
import { ProxyService } from './proxy.service';

describe('ProxyService', () => {
  let service: ProxyService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ProxyService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get fetch results', waitForAsync(() => {
    service.getFetchResults(FAKE_ANSWERS).subscribe(
      (fetchDTO: FetchDTO) => expect(fetchDTO).toBe(FAKE_FETCH)
    );
    const request = httpMock.expectOne('https://beta3.api.climatiq.io/batch');
    expect(request.request.method).toEqual('POST');
    request.flush(FAKE_FETCH);
  }));

});

export const FAKE_ANSWERS: Answer[] = [
  {
    "emission_factor": {
      "_id": "consumer_goods-type_clothing",
      "region": "US"
    },
    "parameters": {
      "money": 1440,
      "money_unit": "eur"
    }
  },
  {
    "emission_factor": {
      "_id": "accommodation_type_hotel_stay",
      "region": "ES"
    },
    "parameters": {
      "number": 40
    }
  }
];

// Mejor las declaramos aquí para que Angular no lo meta en el Bundle
export const FAKE_FETCH: FetchDTO = {
  "results": [
    {
      "co2e": 3196.8985176738884,
      "co2e_unit": "kg",
      "co2e_calculation_method": "ar4",
      "co2e_calculation_origin": "source",
      "emission_factor": {
        "_id": "consumer_goods-type_clothing",
        "source": "EPA",
        "year": "2020",
        "region": "US",
        "category": "Clothing and Footwear",
        "lca_activity": "cradle_to_shelf"
      },
      "constituent_gases": {
        "co2e_total": 3196.8985176738884,
        "co2e_other": null,
        "co2": null,
        "ch4": null,
        "n2o": null
      }
    },
    {
      "co2e": 748.0,
      "co2e_unit": "kg",
      "co2e_calculation_method": "ar4",
      "co2e_calculation_origin": "source",
      "emission_factor": {
        "_id": "accommodation_type_hotel_stay",
        "source": "BEIS",
        "year": "2021",
        "region": "ES",
        "category": "Accommodation",
        "lca_activity": "unknown"
      },
      "constituent_gases": {
        "co2e_total": 748.0,
        "co2e_other": null,
        "co2": null,
        "ch4": null,
        "n2o": null
      }
    },
    {
      "co2e": 1248.576,
      "co2e_unit": "kg",
      "co2e_calculation_method": "ar5",
      "co2e_calculation_origin": "source",
      "emission_factor": {
        "_id": "electricity-energy_source_gas",
        "source": "EXIOBASE",
        "year": "2021",
        "region": "GB",
        "category": "Electricity",
        "lca_activity": "unknown"
      },
      "constituent_gases": {
        "co2e_total": 1248.576,
        "co2e_other": null,
        "co2": null,
        "ch4": null,
        "n2o": null
      }
    }
  ]
};