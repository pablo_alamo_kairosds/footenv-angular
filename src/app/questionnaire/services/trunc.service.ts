import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TruncService {

  truncNumber(value: number): number {
    return Math.round((value + Number.EPSILON) * 100) / 100;
  }

}
