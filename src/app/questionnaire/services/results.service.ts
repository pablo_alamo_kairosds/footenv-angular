import { Injectable } from '@angular/core';
import { ResultsStoreService } from 'src/app/state/results-store.service';
import { Answer, Result } from '../model';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  constructor(private resultsStoreService: ResultsStoreService) { }

  storeResults(answers: Answer[]) {
    const noFetchedResults: Result[] = [];
    const answersToFetch: Answer[] = [];

    answers.forEach((answer: Answer) => {
      if (answer.emission_factor.hasOwnProperty('nofetch')) {
        noFetchedResults.push(this.mapAnswerToResult(answer));
      } else {
        const { category, ...aux } = answer.emission_factor;
        answersToFetch.push({
          parameters: answer.parameters,
          emission_factor: aux
        });

      }
    });

    this.resultsStoreService.init(noFetchedResults);
    this.resultsStoreService.fetchUpdate(answersToFetch);
  }

  mapAnswerToResult(answer: Answer): Result {
    return {
      co2e: answer.parameters.co2e!,
      co2e_unit: answer.parameters.co2e_unit!,
      emission_factor: {
        _id: answer.emission_factor._id!,
        region: answer.emission_factor.region!,
        category: answer.emission_factor.category!
      }
    };
  }

}
