import { TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';
import { TruncService } from './trunc.service';

describe('TruncService', () => {
  let service: TruncService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TruncService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
