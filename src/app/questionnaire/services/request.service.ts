import { Injectable } from '@angular/core';
import { Answer, Question } from '../model';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  execute(questions: Question[], answerInfo: string): Answer {
    const [questionInfo, answerIndex] = answerInfo.split('-');
    const questionIndex = +questionInfo.split('_')[1] - 1;

    const parameters = questions[questionIndex].answers[+answerIndex].parameters;
    const emission_factor = questions[questionIndex].emission_factor;
    return { emission_factor, parameters };
  }

}
