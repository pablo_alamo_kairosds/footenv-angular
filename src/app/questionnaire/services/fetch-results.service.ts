import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { map } from 'rxjs/operators';
import { FetchDTO, ResultDTO } from '../fetch-dto';
import { Answer, Result } from '../model';
import { ProxyService } from './proxy.service';

@Injectable({
  providedIn: 'root'
})
export class FetchService {

  results$: Observable<Result[]>;

  constructor(private proxy: ProxyService) {
    this.results$ = of([]);
  }

  getFetchResults$(answers: Answer[]): Observable<Result[]> {
    this.results$ = this.proxy.getFetchResults(answers).pipe(
      map((batchResults: FetchDTO) => {
        return batchResults.results.map((result: ResultDTO) => {
          return {
            co2e: result.co2e!,
            co2e_unit: result.co2e_unit!,
            emission_factor: result.emission_factor!
          };
        });
      })
    );
    return this.results$;
  }

  hasResults$(): Observable<Result[]> {
    return this.results$;
  }

}
