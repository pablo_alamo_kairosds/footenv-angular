import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';
import { FetchService } from './fetch-results.service';
import { ProxyService } from './proxy.service';

jest.mock('./batch-proxy.service');
describe('FetchService', () => {
  let service: FetchService;
  let proxy: ProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProxyService]
    });
    service = TestBed.inject(FetchService);
    proxy = TestBed.inject(ProxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // it('should map DTO to model', waitForAsync(() => {
  //   const spyProxy = jest.spyOn(proxy, 'getFetchResults').mockImplementation(() => of(FAKE_FETCH));

  //   service.getResults(FAKE_ANSWERS).fetchData$.subscribe(
  //     (FetchDTO: FetchDTO) => {
  //       expect(FetchDTO.results[0].emission_factor._id).toEqual(FAKE_FETCH.results[0].emission_factor._id);
  //       expect(FetchDTO.results[1].co2e_unit).toEqual(FAKE_FETCH.results[1].co2e_unit);
  //     }
  //   );
  //   expect(spyProxy).toHaveBeenCalled();

  //   spyProxy.mockRestore();
  // }));

});
