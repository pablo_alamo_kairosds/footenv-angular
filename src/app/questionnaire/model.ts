export interface Result {
  co2e: number;
  co2e_unit: string;
  emission_factor: EmissionFactor;
}

export interface EmissionFactor {
  _id: string;
  region: string;
  category: string;
  nofetch?: boolean;
  source?: string;
  year?: string;
}

export interface Answer {
  emission_factor: Partial<EmissionFactor>;
  parameters: Parameters;
}

export interface Question {
  _id: string;
  category: string;
  label: string;
  emission_factor: EmissionFactor;
  title: Title;
  answers: QuestionAnswer[];
  hint: Hint;
}

export interface Title {
  en: string;
  es: string;
}

export interface QuestionAnswer {
  text: string;
  parameters: Parameters;
}

export interface Parameters {
  money?: number;
  money_unit?: string;
  number?: number;
  co2e?: number;
  co2e_unit?: string;
}

export interface Hint {
  text: string;
  link: string;
  image: string;
  alt: string;
}