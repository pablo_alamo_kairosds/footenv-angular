const SLIDE_LEAVE_KEYFRAMES: Keyframe[] = [
  { opacity: 1 },
  { opacity: 0 }
];
const SLIDE_ENTER_KEYFRAMES: Keyframe[] = [
  {
    transform: 'translateX(-1rem)',
    opacity: 0
  },
  {
    transform: 'translateX(0)',
    opacity: 1
  }
];

const SLIDE_LEAVE_OPTS: KeyframeAnimationOptions = {
  duration: 300,
  easing: 'ease-in',
  iterations: 1,
  delay: 150,
  fill: 'forwards'
};
const SLIDE_ENTER_OPTS: KeyframeAnimationOptions = {
  duration: 400,
  easing: 'ease-in',
  iterations: 1,
  delay: 600,
  fill: 'forwards'
};

export type AnimationTuple = [Keyframe[], KeyframeAnimationOptions];

const SLIDE_LEAVE: AnimationTuple = [
  SLIDE_LEAVE_KEYFRAMES,
  SLIDE_LEAVE_OPTS
];
const SLIDE_ENTER: AnimationTuple = [
  SLIDE_ENTER_KEYFRAMES,
  SLIDE_ENTER_OPTS
];

export interface MyAnimation {
  slideEnter: AnimationTuple;
  slideLeave: AnimationTuple;
}

export const ANIMATIONS: MyAnimation = {
  slideEnter: SLIDE_ENTER,
  slideLeave: SLIDE_LEAVE
};