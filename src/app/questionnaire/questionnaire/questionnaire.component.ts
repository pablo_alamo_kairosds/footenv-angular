import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import 'genk-interactive-carousel/genk-interactive-carousel.js';
import { ANIMATIONS, MyAnimation } from '../animations';
import { Answer, Question } from '../model';
import { QUESTIONS } from '../questions';
import { ResultsService } from '../services/results.service';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuestionnaireComponent implements OnInit, AfterViewInit {

  @ViewChild('carousel') carousel: ElementRef;

  questions: Question[];
  myAnimations: MyAnimation;
  slideNumber: number;
  numOfSlides: number;

  constructor(private resultsService: ResultsService, private router: Router) { }

  ngOnInit(): void {
    this.questions = QUESTIONS;
    this.myAnimations = ANIMATIONS;
    this.slideNumber = 0;
    this.numOfSlides = 0;
  }

  ngAfterViewInit() {
    this.numOfSlides = this.carousel.nativeElement.childElementCount;
  }

  goToPrevSlide() {
    if (this.slideNumber > 0) {
      this.slideNumber -= 1;
    }
  }

  goToNextSlide() {
    if (this.slideNumber < this.numOfSlides - 1) {
      this.slideNumber += 1;
    }
  }

  storeResults(answers: Answer[]): void {
    this.carousel.nativeElement.animate(...this.myAnimations.slideLeave);
    this.resultsService.storeResults(answers);
    this.router.navigate(['results']);
  }
}
