import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { expect } from '@jest/globals';
import 'genk-interactive-carousel/genk-interactive-carousel.js';
import { of } from 'rxjs';
import { ProxyService } from '../services/proxy.service';
import { FAKE_ANSWERS, FAKE_FETCH } from '../services/proxy.service.spec';
import { QuestionnaireComponent } from './questionnaire.component';

describe('QuestionnaireComponent', () => {
  let component: QuestionnaireComponent;
  let fixture: ComponentFixture<QuestionnaireComponent>;
  let proxy: ProxyService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [QuestionnaireComponent],
      imports: [HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    TestBed.compileComponents();

    fixture = TestBed.createComponent(QuestionnaireComponent);
    component = fixture.componentInstance;
    proxy = TestBed.inject(ProxyService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call batch service with answers', waitForAsync(() => {
    const spyProxy = jest.spyOn(proxy, 'getFetchResults').mockImplementation(() => of(FAKE_FETCH));

    component.storeResults(FAKE_ANSWERS);
    // component.fetchedData$.subscribe(
    //   (FetchDTO: FetchDTO) => {
    //     expect(FetchDTO.results[0].emission_factor._id).toEqual(FAKE_FETCH.results[0].emission_factor._id);
    //     expect(FetchDTO.results[1].co2e_unit).toEqual(FAKE_FETCH.results[1].co2e_unit);
    //   }
    // );
    expect(spyProxy).toHaveBeenCalled();

    spyProxy.mockRestore();
  }));

});
