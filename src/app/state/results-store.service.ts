import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Answer, Result } from '../questionnaire/model';
import { FetchService } from '../questionnaire/services/fetch-results.service';
import { ObservableResultsService } from '../questionnaire/services/observable-results.service';
import { Store } from './state';

@Injectable({
  providedIn: 'root'
})
export class ResultsStoreService extends Store<Result[]> {

  constructor(private fetchService: FetchService,
    private observableResultsService: ObservableResultsService) {
    super();
  }

  init(results: Result[]): void {
    lastValueFrom(this.observableResultsService.getResults$(results).pipe(tap(this.store)));
  }

  fetchUpdate(answers: Answer[]): void {
    lastValueFrom(
      this.fetchService.getFetchResults$(answers).pipe(
        tap(results => this.store([...this.get(), ...results]))
      )
    );
  }

}
