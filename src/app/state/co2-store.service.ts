import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Result } from '../questionnaire/model';
import { Co2Service } from '../results/services/co2.service';
import { Store } from './state';

@Injectable({
  providedIn: 'root'
})
export class Co2StoreService extends Store<number> {

  constructor(private co2Service: Co2Service) {
    super();
  }

  init(results: Result[]): void {
    lastValueFrom(this.co2Service.getCO2$(results).pipe(tap(this.store)));
  }

}
