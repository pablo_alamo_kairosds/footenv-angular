export const HOME_CARDS_DATA = [
  {
    title: "Sustainability",
    image: "assets/images/positive/positive1.png",
    imgAlt: "Sustainable planet earth",
    description: "Balance between the human being and the resources of his environment.",
  },
  {
    title: "Reforestation",
    image: "assets/images/positive/positive2.png",
    imgAlt: "Flower in a hand",
    description: "Planting billions of trees could be a solution to fight climate change",
  },
  {
    title: "Green companies",
    image: "assets/images/positive/positive3.png",
    imgAlt: "Green companies",
    description: "A green company claims to act in a way which minimizes damage to the environment.",
  },
  {
    title: "Clean energies",
    image: "assets/images/positive/positive4.png",
    imgAlt: "Solar panel",
    description: "Clean energy produce power without having negative environmental impacts.",
  },
  {
    title: "Eletric cars",
    image: "assets/images/positive/positive5.png",
    imgAlt: "Eletric car",
    description: "Electric cars are energy efficient and net good for the environment.",
  },
  {
    title: "Reduce, Reuse, Recycle",
    image: "assets/images/positive/positive6.png",
    imgAlt: "Recycle symbol",
    description: "It helps the community, and the environment by saving money, energy, and natural resources. ",
  },
];
