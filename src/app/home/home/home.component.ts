import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { QUESTIONS } from 'src/app/questionnaire/questions';
import { ResultsStoreService } from 'src/app/state/results-store.service';
import { HomeCard } from './home-card-model';
import { HOME_CARDS_DATA } from './home-cards-data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {

  hasResults: boolean;
  homeCards: HomeCard[];

  constructor(private store: ResultsStoreService) { }

  ngOnInit(): void {
    this.hasResults = Object.keys(this.store.get()).length === QUESTIONS.length;
    this.homeCards = HOME_CARDS_DATA;
  }

}
