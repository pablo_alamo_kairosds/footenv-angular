export interface HomeCard {
  title: string;
  image: string;
  imgAlt: string;
  description: string;
}
